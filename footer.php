<footer>
    <div class="container">
        <div class="footer-wrap">
            <div class="footer-wrap__left">
                  <a href="<?php echo get_home_url(); ?>">  <img src=" <?php echo get_field('logo', 'option')['url'];?>" alt=""></a>
            </div>
            <div class="footer-wrap__right">
                <div class="newsletter">
                    <p>
                        Do you want to see a demo of the app?
                    </p>
                    <div class="newsletter-input">
                        <input type="text" placeholder="pharmin@pharmin.sk">
                        <a href="">Send</a>
                       
                    </div>
                    <div class="newsletter-input__checkbox">
                        <input type="checkbox"> <span>I agree with <strong> terms and conditions</strong>  and with <strong> GDPR</strong>  </span>                       
                    </div>
                    <div class="newsletter-input__checkbox">
                         <input type="checkbox"> <span>I agree with subscribing to newsletter</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-footer">
                <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'secondary_menu',
                                    'menu_id'        => 'secondary_menu',
                                    'menu_class' 		 => 'secondary_menu'
                                )
                            );
                        ?>
            </div>
        <div class="copy-wrap">
            <div class="copy-wrap__info">
                <p>
                    Copyright © Pharm-In 2020. All rights reserved
                </p>
            </div>
            
            <div class="copy-wrap__menu">
                <ul>
                    <li>GDPR</li>
                    <li>Cookies</li>
                </ul>
            </div>
        </div>
        
    </div>
</footer>























<?php wp_footer(); ?>
</body>
</html>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php bloginfo('description');?></title>
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css?v=<?php echo time(); ?>">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">


	<?php wp_head(); ?>
</head>
<body>
	<header class="header">
		<div class="header-top">
			<div class="container">
				<div class="header-top__wrap">
					<ul>
						<li>Request demo</li>
						<li>  About Pharmin</li>
						<li> Kontakt</li>
						<li class="login"> <img src="<?php echo get_template_directory_uri() . '/images/lock.svg' ?>" alt=""> Login</li>
					</ul>

				</div>
			</div>
		</div>
		<div class="header-main">
			<div class="container">
				<div class="header-main__wrap">
					<div class="logo">
						   <a href="<?php echo get_home_url(); ?>">  <img src=" <?php echo get_field('logo', 'option')['url'];?>" alt=""></a>
					</div>
					<div class="nav">
						<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( '', 'gombik-theme' ); ?>
								<span class="menu__btn-bar"></span>
                            	<span class="menu__btn-bar"></span>
                          	   <span class="menu__btn-bar"></span>
                   		       <span class="menu__btn-bar"></span>
					
						</button>
						 <a class="home-btn" href="<?php echo get_home_url(); ?>">  <img src="<?php echo get_template_directory_uri() . '/images/home.svg' ?>" alt=""> </a>
						<?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'primary_menu',
                                    'menu_id'        => 'primary-menu',
                                    'menu_class' 		 => 'menu primary-menu'
                                )
                            );
                        ?>
					</div>
					<div class="header-btn">
						<a>Request demo</a>
					</div>

				</div>

			</div>
		</div>
	</header>
<?php 

 /* Template name: Domov */

get_header();?>

<section class="hero">
    <div class="container">
        <div class="hero-wrap">
            <div class="hero-wrap__left">
                <h1>
                        Drug prices from 28 countries, updated daily, in one user friendly app
                </h1>
                <p>
                    <?php the_field('hero_info');?>
                </p>
                <div class="hero-btn">
                    
                    <a href="" class="hero-btn__wrap">
                        <img src=" <?php echo get_template_directory_uri() . '/images/cursor.svg' ?>" alt="">
                       Request demo
     
                    </a>
                </div>
            </div>
            <div class="hero-wrap__right">
                <img src="<?php echo get_field('main_img')['url'];?>" alt="">
            </div>
        </div>
    </div>
</section>

<section class="feature" id="features"> 
    <div class="container">
        <div class="feature-box__wrap">
          
            <?php
                                    if(have_rows('feature') ):
                                        while( have_rows('feature') ) : the_row();
                                    ?>
                                   
                                     <div class="feature-box">
                                       
                                      
                                        <p>   <i class="far fa-check-circle"></i><?php the_sub_field('feature_info');?></p>
                                    </div>
                                                        
                                  
                                <?php
                                    endwhile;
                                    endif;
                                ?>
        </div>
    </div>
</section>

<section class="benefits">
    <div class="container">
        <div class="main-title">
            <h2>What Can ExPrice do for you?</h2>
        </div>
         <?php
            if(have_rows('benefits') ):
                while( have_rows('benefits') ) : the_row();
            ?>                               
                <div class="benefits-box">
                    <div class="benefits-box__img">
                        <div class="benefits-box__img-wrap">
                            <img src="<?php echo get_sub_field('benefits_image')['url'];?>" alt="">
                        </div>  
                    </div>
                    <div class="benefits-box__content">
                        <div class="benefits-box__content-title">
                            <h3>
                                <?php the_sub_field('benefits_title');?>
                            </h3>
                        </div>
                        <div class="benefits-box__content-info">
                            <p>
                                <?php the_sub_field('benefits_content');?>
                            </p>
                        </div>
                    </div>
                </div>                                                                       
            <?php
                endwhile;
                endif;
            ?>
    </div>
    <div class="container">
        <div class="top-newsletter">
            <div class="top-newsletter__left">
                <i class="far fa-envelope"></i>
                <p>
                    I would like to see a demo of the app!
                </p>
            </div>
            <div class="top-newsletter__right">
                     <div class="newsletter-input">
                        <input type="text" placeholder="pharmin@pharmin.sk">
                        <a href="">Send</a>                    
                    </div>
                     <div class="newsletter-input__checkbox">
                        <input type="checkbox"> <span>I agree with <strong> terms and conditions</strong>  and with <strong> GDPR</strong>  </span>                       
                    </div>
                    <div class="newsletter-input__checkbox">
                         <input type="checkbox"> <span>I agree with subscribing to newsletter</span>
                    </div>
               

            </div>
        </div>
    </div>
</section>
<section class="offers">
    <div class="container">
        <div class="offers-title">
            <h3>Key features that Ex-PRICE offers</h3>
        </div>
        <div class="offers-wrap">
            <div class="offers-wrap__left">
                <ul>
                      <?php
                     
                        for ($x = 1; $x <=8; $x++){ 
                            if(have_rows('offers') ):
                                while( have_rows('offers') ) : the_row();
                                
                        ?>
                                   
                                
                             <li id="<?php echo $x++?>"> <i class="fas fa-chevron-down"></i> <?php the_sub_field('offers_title');?></li>     
                          
                            
                                  
                        <?php
                                endwhile;
                            endif;
                        }
                        ?>
                   
                </ul>
            </div>
            <div class="offers-wrap__right">
                <?php
                 for ($x = 1; $x <=8; $x++){ 
                            if(have_rows('offers') ):
                                while( have_rows('offers') ) : the_row();

                              
                                
                               
                               
                        ?>
                                   
                              
                            <div data-box="<?php echo $x++?>" class="offers-wrap__right-box">
                         
                                <h4><?php the_sub_field('offers_title');?></h4>
                                <p>
                                    <?php the_sub_field('offers_content');?>
                                </p>
                                <div class="offers-btn">
                                    <a href="#"> <i class="fas fa-chevron-left"></i> <?php the_sub_field('offers_title');?></a>
                                     <a href="#">  How many databases? <i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                             
                           
                                  
                        <?php
                                endwhile;
                            endif;
                        }
                        ?>
              
                
                    
            </div>
        </div>
    </div>

</section>


<section class="plan" id="pricing">
    <div class="container">
        <div class="plan-title">
            <h4>Choose a plan that suits you best. </h4>
        </div>
         <div class="plan-box__wrap">
          <?php
            if(have_rows('plans') ):
                while( have_rows('plans') ) : the_row();                                                                         
            ?>                        
                           
                <div class="plan-box <?php the_sub_field('important')?>"> 
                     
                    <h3 class="plan-box__title">  <?php the_sub_field('title');?></h3>
                    <p class="plan-box__desc">  <?php the_sub_field('info');?></p>
                    <div class="plan-box__group">
                        <h3><?php the_sub_field('price');?></h3>
                        <p>monthly for 1 use</p>
                    </div>
                    <ul>
                         <?php
                                    if(have_rows('plans_items') ):
                                        while( have_rows('plans_items') ) : the_row();
                                    ?>
                                   
                                    <li><img src="<?php echo get_template_directory_uri() . '/images/check-green.svg' ?>" alt=""><?php the_sub_field('add_item_info');?> </li>
                                                        
                                  
                                <?php
                                    endwhile;
                                    endif;
                                ?>
                     
                    </ul>
                    <ul>
                         <?php
                                    if(have_rows('plans_items_no_support') ):
                                        while( have_rows('plans_items_no_support') ) : the_row();
                                    ?>
                                   
                                    <li><img src="<?php echo get_template_directory_uri() . '/images/error.svg' ?>" alt=""><?php the_sub_field('add_new_item');?> </li>
                                                        
                                  
                                <?php
                                    endwhile;
                                    endif;
                                ?>
                     
                            
                    </ul>
                    <div class="plan-box__btn">
                        <a href="#"> <?php the_sub_field('button');?></a>
                    </div>
                </div>
                                                                                                                               
        <?php
                endwhile;
            endif;
        ?>
        </div>
    </div>
</section>
<section class="contact">
    <div class="container">
        <div class="contact-wrap">
            <div class="contact-wrap__left">
                <div class="person">
                    <div class="person-img">
                        <img src="<?php echo get_field('contact_img')['url'];?>" alt="">
                    </div>
                    <div class="person-info">
                        <h5 class="person-info__name"> <?php the_field('contact_name');?></h5>
                        <p class="person-info__position"> <?php the_field('contact_position');?></p>
                        <a href="">LinkdIn</a>
                    </div>
                </div>
            </div>
             <div class="contact-wrap__right">
                <div class="contact-input-1">
                    <input type="text" placeholder="Select your type:">
                </div>
                <div class="contact-input-2">
                    <input type="text" placeholder="Name">
                    <input type="text" placeholder="E-mail">
                </div>
                <div class="contact-input-3">
                    <input type="text" placeholder="Notes">
                </div>
                <div class="contact-checkbox">
                    <input type="checkbox"><span>I agree with terms and conditions and with GDPR  </span>
                </div>
                <div class="contact-btn">
                    <a> <img src="<?php echo get_template_directory_uri() . '/images/cursor.svg' ?>" alt=""> Request demo </a>
                </div>
            </div>
        </div>
    </div>

</section>




<?php get_footer();
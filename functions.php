<?php
    if ( !defined('_S_VERSION')){
            define('_S_VERSION', '1.0.0');
    }

    add_theme_support( 'menus' ); 
    add_theme_support( 'post-thumbnails' );
    
    if (function_exists('acf_add_options_page')) {

        acf_add_options_page(array(
            'page_title' 	=> 'Nastavenie témy',
            'menu_title'	=> 'Nastavenie témy',
            'menu_slug' 	=> 'theme-settings'
        ));
    }
    if ( ! function_exists( 'grandaliro_register_nav_menu' ) ) {
 
        function grandaliro_register_nav_menu(){
            register_nav_menus( array(
                'primary_menu' => __( 'Primary Menu', 'grandaliro-theme' ),
                'secondary_menu'  => __( 'Secondary Menu', 'grandaliro-theme' ),
                
                
            ) );
        }
        add_action( 'after_setup_theme', 'grandaliro_register_nav_menu', 0 );
    }
       

    
    /*SCRIPTS AND STYLE */
    function grandaliro_theme_scripts() {
	//wp_enqueue_style( 'grandaliro-theme-style', get_stylesheet_uri(), array(), _S_VERSION );
	//wp_style_add_data( 'grandaliro-theme-style', 'rtl', 'replace' );


    //Jquery
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), _S_VERSION, true);
	

	//styles
	

	
	wp_enqueue_script( 'grandaliro-theme-animations', get_template_directory_uri() . '/js/animations.js', array(), _S_VERSION, true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'grandaliro_theme_scripts' );